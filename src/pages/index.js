import React, { Component } from "react";
import axios from "axios";

import Layout from "../components/Layout";
import LocalDate from "../components/LocalDate";
import AcademicDay from "../components/AcademicDay";

class IndexPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      academicDay: "",
    };

    this.getAcademicDay = this.getAcademicDay.bind(this);
  }

  componentDidMount() {
    this.getAcademicDay().then(day => {
      this.setState({ academicDay: day });
    });
  }

  getAcademicDay() {
    const noCorsServer = "https://cors-anywhere.herokuapp.com";
    const dayEndpoint =
      "http://www.mbs.edu.co/portal/sisencuentra/cal-academ.php";

    return axios
      .get(`${noCorsServer}/${dayEndpoint}`)
      .then(resp => resp.data.match(/\d/g)[0]);
  }

  render() {
    return (
      <Layout>
        <main className="index-page">
          <LocalDate locale="es-CO" />
          <AcademicDay day={this.state.academicDay} />
        </main>
      </Layout>
    );
  }
}

export default IndexPage;
