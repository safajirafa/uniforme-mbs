import React from "react";
import { Link} from "gatsby";
import logoMbs from "../../images/logombs.png";

const Header = () => (
  <header>
    <Link to="/">
      <img src={logoMbs} alt="logo mbs" className="logo-mbs"/>  
    </Link>
  </header>
);

export default Header;
