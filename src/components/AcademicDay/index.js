import React, { Component } from "react";
import PropTypes from "prop-types";

import SwimmingDay from "../SwimmingDay";
import FormalUniformDay from "../FormalUniformDay";

class AcademicDay extends Component {
  constructor(props) {
    super(props);

    this.isFormalUniformDay = this.isFormalUniformDay.bind(this);
    this.isSwimmingDay = this.isSwimmingDay.bind(this);
  }

  isFormalUniformDay(day) {
    return day === "6";
  }

  isSwimmingDay(day) {
    return day === "2";
  }

  render() {
    const day = this.props.day;
    return (
      <div>
        <span>D&iacute;a acad&eacute;mico: </span>
        <strong>{day || "Cargando..."}</strong>
        {this.isFormalUniformDay(day) &&
          <FormalUniformDay />
        }
        {this.isSwimmingDay(day) &&
          <SwimmingDay />
        }
        {day === -1 && (
          <div>Parece que no hay info cargada para esta fecha 😱</div>
        )}
      </div>
    );
  }
}

AcademicDay.propTypes = {
  day: PropTypes.string,
};

export default AcademicDay;
