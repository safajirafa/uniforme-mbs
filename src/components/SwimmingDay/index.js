import React from "react";

import Swimming from "../../images/swimming.png";
import Skating from "../../images/skating.png";

const SwimmingDay = () =>
  <div className="sports-container">
    <div>
      <h4>Hoy tienes natacion!</h4>
      <img src={Swimming} alt="swim" style={{ width: "25%" }}/>
    </div>
    <div>
      <h4>Y tambien tienes patinaje</h4>
      <img src={Skating} alt="skate" style={{ width: "25%" }}/>
    </div>
  </div>;

export default SwimmingDay;
