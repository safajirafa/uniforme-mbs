import React from "react";
import Necktie from "../../images/necktie.png";

const FormalUniformDay = () =>
  <div>
    <h4>Hoy es uniforme de gala</h4>
    <img src={Necktie} alt="necktie"/>
  </div>;

export default FormalUniformDay;
