import React from "react";
import PropTypes from "prop-types";

const LocalDate = props => (
  <h4>Hoy es {new Date().toLocaleDateString(props.locale, props.dateOpts)}</h4>
);

LocalDate.defaultProps = {
  locale: "en-US",
  dateOpts: {
    day: "numeric",
    month: "long",
  },
};

LocalDate.propTypes = {
  locale: PropTypes.string.isRequired,
  dateOpts: PropTypes.object.isRequired,
  day: PropTypes.string,
};

export default LocalDate;
