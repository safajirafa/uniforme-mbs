import React, { Fragment } from "react";
import Helmet from "react-helmet";
import Header from "../Header";

import "./layout.scss";

const TemplateWrapper = ({ children }) => (
  <Fragment>
    <Helmet
      title="Uniforme MBS"
      meta={[
        { name: "description", content: "Montessori British School" },
        { name: "keywords", content: "MBS, uniforme" },
      ]}
    />
    <Header />
    {children}
  </Fragment>
);

export default TemplateWrapper;
