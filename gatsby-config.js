module.exports = {
  siteMetadata: {
    title: "Uniforme MBS"
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: "Uniforme MBS",
        short_name: "MBS",
        description: "Con esta app puedes saber que uniforme tienes que llevar segun el dia de la semana",
        start_url: "/",
        display: "standalone",
        orientation: "portrait",
        theme_color: "#db1a1a"
      }
    }
  ]
};
